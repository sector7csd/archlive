# Archlive

Arch Linux Live/Rescue CD with quality of life features, built with [Archiso](https://wiki.archlinux.org/index.php/Archiso).

This is based on the on the video [Arch Linux: Create Your Own Installer](https://www.youtube.com/watch?v=-yPhW5o1hNM) from [EF - Tech Made Simple](https://www.youtube.com/c/EFTechMadeSimple). Thank you very much for the inspiring video.

It was mainly created to provide an environment to install [Arch Linux](https://www.archlinux.org/). Fits perfectly together with my [Salei project](https://bitbucket.org/sector7csd/salei/src/master/).

Passwort for the `archlive` user is `archlive`.

![Arch Live Screenshot](https://bitbucket.org/sector7csd/archlive/raw/2d6f6ff793371500b0208793770213961c334804/screenshots/ArchLive.png)

## Features

- **releng** profile based: familiar tools from offical Arch Linux ISO
- **Cinnamon desktop** for simple and intuitive user experience
- **GUI tools** included
- **Java Runtimes** included

## Requirements

All you need is to have the [Archiso](https://wiki.archlinux.org/index.php/Archiso) package installed:

```bash
sudo pacman -S archiso
```

## Building

It's recommended to build in a separate virtual machine. It can be dangerous to **rm -rf out/ work/** to clean, because it could be the case that there a links to your root file system. The build tool requires a machine with at least 8 GB of memory.

```bash
mkarchiso -v -w ATemporarlyBuildDirectory -o YourIsoOutputDirectory YourArchLiveDirectory
```
