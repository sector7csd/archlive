#!/usr/bin/env bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

function addUserToGroup() {
	
	# adds a user to a group
	# $1 user name
	# $2 group name
	
	usermod -a -G "$2" "$1"
}

function createUser() {
	
	# creates a new user
	# $1 user name
	# $2 full user name
	# $3 shell
	# $4 password
	# $5 sudo permission
	
	groupadd "$1"
	useradd -m -g "$1" -G users,audio,video,storage,power,floppy,input,optical,scanner -s "$3" -c "$2" "$1"
	echo "$1:$4" | chpasswd
	
	if [ "$5" == "sudo" ]; then
		addUserToGroup "$1" "wheel"
	fi
}

set -e -u

# Warning: customize_airootfs.sh is deprecated! Support for it will be removed in a future archiso version.

# enable sudo
echo '%wheel ALL=(ALL:ALL) ALL' > /etc/sudoers.d/wheel_group

# create users
createUser "archlive" "archlive user" "/bin/bash" "archlive" "sudo"

# setup locale
sed -i 's/#\(en_US\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

# setup mirror list
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

# setup slim auto login
sed -i 's/#auto_login          no/auto_login          yes/g' /etc/slim.conf
sed -i 's/#default_user        simone/default_user        archlive/g' /etc/slim.conf
sed -i 's/current_theme       default/current_theme       archlinux-simplyblack/g' /etc/slim.conf
